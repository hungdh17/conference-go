from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from .models import ConferenceVO

# from .api_views import ConferenceVODetailEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceVODetailEncoder()}
    # encoders = conference": ConferenceDetailEncoder
    # ******** do i use detail or list??


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        # "href"
    ]


@require_http_methods(["POST", "GET"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # attendees = [
    #     {
    #         "name": a.name,
    #         "href": a.get_api_url()
    #     }
    #     for a in Attendee.objects.filter(conference=conference_id)]
    # return JsonResponse({"attendees": attendees})

    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            # except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
    attendee = Attendee.objects.create(**content)
    return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)
    # another way
    # attendees = Attendee.objects.filter(conference=conference_id)

    # return JsonResponse({"attendees": [{
    #     "name": attendee.name,
    #     "href": attendee.get_api_url(),
    #     } for attendee in attendees]})


# def api_list_attendees(request, conference_id):
#     response = []
#     attendees = Attendee.objects.all()
#     for a in attendees:
#         response.append(
#             {
#                 "name": a.name
#             }
#         )
# 	return JsonResponse({"attendees": response})

# def api_list_attendees(request, conference_id):
#     attendees = [
#         {
#             "name": a.name,
#         }
#         for a in Attendee.objects.filter(conference=conference_id)
#     ]
# 	return JsonResponse({"attendees": attendees})


# response = []
# attendees = Attendee.objects.all()
# for a in attendees:
#     response.append(
#         {
#             "name": a.name
#         }
#     )
# these both list the attendees but idk which one is better practice vvvvv
# attendees = [
#     {
#         "name": a.name,
#     }
#     for a in Attendee.objects.filter(conference=conference_id)
#     # for a in Attendee.objects.get(all)
# ]
# return JsonResponse({"attendees": response})
# # return JsonResponse({"attendees": attendees})
# # attendees = [
# #     {
# #         # "email": a.email,
# #         "name": a.name,
# #         # "company_name": a.company_name,
# #         # "created": a.created,
# #         # "conference": a.conference

# #     }
# #     for a in Attendee.objects.filter(conference=conference_id)
# #     # for a in Attendee.objects.get(all)
# # ]


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse({
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     }
    # })
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
