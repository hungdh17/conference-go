import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# approvals
def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    # email_body = To: the presenter's email address
    # From: use the fake email address "admin@conference.go"
    # Subject: "Your presentation has been accepted"
    # Body: "{name}, we're happy to tell you that your presentation {title} has been accepted"
    message = json.loads(body)
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been accepted"
    body = f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted"
    send_mail(
        subject,  # "Subject here",
        body,  # "Here is the message.",
        "admin@conference.go",  # "from@example.com",
        # f"{presenter_email}",
        [presenter_email],  # ["to@example.com"],
        fail_silently=False,
    )
    # send_mail(
    "Subject here",
    "Here is the message.",
    "from@example.com",
    #     ["to@example.com"],
    #     fail_silently=False,
    # )
    print("Sent approval email.")


# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue="presentation_approvals")
# channel.basic_consume(
#     queue="presentation_approvals",
#     on_message_callback=process_approval,
#     auto_ack=True,
# )
# channel.start_consuming()

# rejections
def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    # email_body = To: the presenter's email address
    # From: use the fake email address "admin@conference.go"
    # Subject: "Your presentation has been accepted"
    # Body: "{name}, we're happy to tell you that your presentation {title} has been accepted"
    message = json.loads(body)
    presenter_email = message["presenter_email"]
    presenter_name = message["presenter_name"]
    title = message["title"]
    subject = "Your presentation has been rejected"
    body = f"{presenter_name}, we regret to tell you that your presentation {title} has been rejected"
    send_mail(
        subject,  # "Subject here",
        body,  # "Here is the message.",
        "admin@conference.go",  # "from@example.com",
        # f"{presenter_email}",
        [presenter_email],  # ["to@example.com"],
        fail_silently=False,
    )
    # send_mail(
    "Subject here",
    "Here is the message.",
    "from@example.com",
    #     ["to@example.com"],
    #     fail_silently=False,
    # )
    print("Sent rejection email.")


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)


channel.start_consuming()


# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue="presentation_approvals")
# channel.basic_consume(
#     queue="presentation_approvals",
#     on_message_callback=process_approval,
#     auto_ack=True,
# )
# channel.queue_declare(queue="presentation_rejections")
# channel.basic_consume(
#     queue="presentation_rejections",
#     on_message_callback=process_rejection,
#     auto_ack=True,
# )
# channel.start_consuming()
