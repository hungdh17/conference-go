from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
from json import loads
import requests

# from django.http import JsonResponse


def get_photo(city, state):
    # how to add a header as part of the get request (https://requests.readthedocs.io/en/latest/user/quickstart/#custom-headers)
    # create dictionary for the headers to use in request

    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    # create url for request with city and state
    url = "https://api.pexels.com/v1/search"
    # make the request
    response = requests.get(url, params=params, headers=headers)
    # parse the json response, the content is the entire dictionary that's been sent
    content = json.loads(response.content)
    try:
        # return a dictionary that contains a `picture_url` key and one of the urls for one of the pictures in the response
        return {"picture_url": content["photos"][0]["src"]["original"]}
    # chooses 1 picture
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # same as above^but with weather api key
    location_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    # location url base pattern:
    # url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    location_url = "http://api.openweathermap.org/geo/1.0/direct?"
    # make the request
    response = requests.get(location_url, params=location_params)
    # parse JSON response
    location = json.loads(response.content)
    # get latitude and longitude from response
    try:
        lat = location[0]["lat"]
        lon = location[0]["lon"]
    except (KeyError, IndexError):
        return None

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Make the request
    weather_response = requests.get(weather_url, params=weather_params)
    # Parse the JSON response
    weather = json.loads(weather_response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    try:
        temp = weather["main"]["temp"]
        # calculate farenheit
        # line 58 if omitted: need to change to imperial units
        # temp = f"Temperature: {round((temp - 273.15) * (9 / 5) + 32)} degrees."
        temp = f"Temperature: {temp} degrees"
        desc = weather["weather"][0]["description"]
        desc = f"Weather: {desc}."
        # Return the dictionary
        return {"temp": temp, "desc": desc}
    except (KeyError, IndexError):
        return {"weather": None}
    # weather_dict = {
    #     "temp(F)": weather["main"]["temp"],
    #     "Weather Description": weather["weather"][0]["description"],
    # }
    # # Return the dictionary
    # return weather_dict
